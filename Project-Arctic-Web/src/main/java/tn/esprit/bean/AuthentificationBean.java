package tn.esprit.bean;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import tn.esprit.entities.Employe;
import tn.esprit.services.IEmployeLocal;

@ManagedBean 
@SessionScoped
public class AuthentificationBean {

	@EJB
	IEmployeLocal iEmployeLocal;
	
	private Employe employe;
	private String login;
	private String password;
	private String resultat="";

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Employe getEmploye() {
		return employe;
	}

	public void setEmploye(Employe employe) {
		this.employe = employe;
	}
	
	public String doLogin(){
		String navigateTo= null;
		employe = iEmployeLocal.getEmployeByLoginANDPassword(login, password);
		if (employe != null) {
			resultat = employe.getName();
			navigateTo= "/welcomeTemplate?faces-redirect=true";
		}
		else {
			FacesContext.getCurrentInstance().addMessage("form:btn", new FacesMessage("Bad Connexion"));
		}
		return navigateTo;
		
	}
	
	public String logOut(){
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
		return "/indexTemplate?faces-redirect=true";
	}

	public String getResultat() {
		return resultat;
	}

	public void setResultat(String resultat) {
		this.resultat = resultat;
	}
	
	
	
}
