package tn.esprit.tests;

import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import tn.esprit.entities.Employe;
import tn.esprit.entities.Ingenieur;
import tn.esprit.entities.Projet;
import tn.esprit.entities.Technicien;
import tn.esprit.services.IEmployeRemote;
import tn.esprit.services.ProjectInterfaceRemote;

public class ProjetTest {

	public static void main(String[] args) throws NamingException {
		// TODO Auto-generated method stub
		Context ctx = new InitialContext();

		ProjectInterfaceRemote proj = (ProjectInterfaceRemote) ctx.lookup("Project-Arctic-EAR/Project-Arctic/ProjetService!tn.esprit.services.ProjectInterfaceRemote");
		IEmployeRemote proxy = (IEmployeRemote) ctx.lookup("Project-Arctic-EAR/Project-Arctic/EmployeService!tn.esprit.services.IEmployeRemote");
		
		Ingenieur a = (Ingenieur) proxy.findEmployeById(11);
		
		Ingenieur e = new Ingenieur();
		e.setName("sofien");
		e.setLogin("sbh");
		e.setLastName("bhammadi");
		
		Employe e1 = new Technicien();
		e1.setName("mohamed");

		((Technicien) e1).setDepartement("Info");
		
		Projet p = new Projet();
		p.setNom("p25");
		p.setEmploye(a);
		Projet p1 = new Projet();
		p1.setNom("p11");
		p1.setEmploye(e);
		Projet p11 = new Projet();
		p11.setNom("p12");
		p11.setEmploye(e);
		proj.addProject(p11);
		proj.addProject(p1);
		proj.updateProjet(p);
		
		List<Projet> lproj= proj.getAllProjets();
//		
	lproj.forEach(pr->System.out.println(pr.getNom()));
//		// identique
//		for (Projet projet : lproj) {
//			System.out.println(projet.getNom());
//		}

	// modifier l'id selon l'existant dans votre base de donn�es
		Projet p4= proj.getProjetById(8);
		
		System.out.println(p4.getNom()+" responsable : "+p4.getEmploye().getName());
	}

}
