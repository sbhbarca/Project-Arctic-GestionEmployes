package tn.esprit.tests;

import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import tn.esprit.entities.Employe;
import tn.esprit.entities.Ingenieur;
import tn.esprit.entities.Projet;
import tn.esprit.entities.Technicien;
import tn.esprit.services.IEmployeRemote;
import tn.esprit.services.ProjectInterfaceRemote;

public class ClientTest {

	public static void main(String[] args) throws NamingException {
		// TODO Auto-generated method stub
		Context ctx = new InitialContext();
		IEmployeRemote proxy = (IEmployeRemote) ctx.lookup("Project-Arctic-EAR/Project-Arctic/EmployeService!tn.esprit.services.IEmployeRemote");
		
		ProjectInterfaceRemote proj = (ProjectInterfaceRemote) ctx.lookup("Project-Arctic-EAR/Project-Arctic/ProjetService!tn.esprit.services.ProjectInterfaceRemote");
		
		// test ajout
		
		Ingenieur e = new Ingenieur();
		e.setName("Salem");
		e.setGrade("Chef Projet");

		Employe e1 = new Technicien();
		e1.setName("mohamed");

		((Technicien) e1).setDepartement("Info");

		proxy.addEmploye(e1);
		proxy.addEmploye(e);

		// affichage employ�
		
		List<Employe> lemp = proxy.findAllEmploye();

		lemp.forEach(e2 -> System.out.println(e2.getName()));
		// identique
		for (int i = 0; i < lemp.size(); i++) {
			System.out.println(lemp.get(i).getName());
		}

//		// supprimer employ� 
	
//		proxy.deleteemploye(51);
		
		// affichage des projets par responsable
		
		// modifier l'id selon l'existant dans votre base de donn�es
		Ingenieur employe = (Ingenieur) proxy.findEmployeById(11);
		
		List<Projet> lp = employe.getProjets();
		System.out.println(employe.getName());
		System.out.println("List des projets by responsable : ");
		for (Projet projet : lp) {
			System.out.println(projet.getNom());
		}
		
		
		//listes des projets by Employ� (relation @ManyToMany)
		List<Projet> lpe= proj.getProjetsByEmploye(employe);
		System.out.println("List des projets by employe : ");
		for (Projet projet : lpe) {
			System.out.println(projet.getNom());
		}
		
		
		
	}

}
