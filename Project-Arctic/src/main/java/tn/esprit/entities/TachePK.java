package tn.esprit.entities;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class TachePK implements Serializable {

	private int idprojet;
	private int idemploye;
	public int getIdprojet() {
		return idprojet;
	}
	public void setIdprojet(int idprojet) {
		this.idprojet = idprojet;
	}
	public int getIdemploye() {
		return idemploye;
	}
	public void setIdemploye(int idemploye) {
		this.idemploye = idemploye;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idemploye;
		result = prime * result + idprojet;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TachePK other = (TachePK) obj;
		if (idemploye != other.idemploye)
			return false;
		if (idprojet != other.idprojet)
			return false;
		return true;
	}
	
	
}
