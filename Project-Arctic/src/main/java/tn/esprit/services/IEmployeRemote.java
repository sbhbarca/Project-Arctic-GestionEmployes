package tn.esprit.services;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.entities.Employe;
import tn.esprit.entities.Projet;

@Remote
public interface IEmployeRemote {
	
	
	public void addEmploye(Employe e);
	public Employe updateemploye(Employe e);
	public void deleteemploye(int id);
	public Employe findEmployeById(int id);
	public List<Employe> findAllEmploye();
	public List<Employe> findEmployeContainsName(String name);
	public List<Employe> findEmployeByName(String name);
	public Employe getResponsableByProjet(Projet p);

}
