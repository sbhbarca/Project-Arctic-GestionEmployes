package tn.esprit.services;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Stateful;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import tn.esprit.entities.Employe;
import tn.esprit.entities.Projet;

@Stateless
//@Stateful
//@Singleton
public class EmployeService implements IEmployeLocal, IEmployeRemote {

	@PersistenceContext
	EntityManager em;
	
	@Override
	public void addEmploye(Employe e) {
		// TODO Auto-generated method stub
		em.persist(e);
	}

	@Override
	public Employe updateemploye(Employe e) {
		// TODO Auto-generated method stub
		return em.merge(e);
	}

	@Override
	public void deleteemploye(int id) {
		// TODO Auto-generated method stub
		em.remove(findEmployeById(id));
	}

	@Override
	public Employe findEmployeById(int id) {
		// TODO Auto-generated method stub
		return em.find(Employe.class, id);
	}

	@Override
	public List<Employe> findAllEmploye() {
		// TODO Auto-generated method stub
		Query req = em.createQuery("select e from Employe e");
		
		return req.getResultList();
	}

	@Override
	public List<Employe> findEmployeContainsName(String nom) {
		// TODO Auto-generated method stub
		Query req= em.createQuery("select e from Employe e where name like :name");
		req.setParameter("name", "%"+nom+"%");
		return req.getResultList();
	}

	@Override
	public List<Employe> findEmployeByName(String nom) {
		// TODO Auto-generated method stub
		Query req= em.createQuery("select e from Employe e where name like :name");
		req.setParameter("name", nom);
		return req.getResultList();
	}

	@Override
	public Employe getResponsableByProjet(Projet projet) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Employe getEmployeByLoginANDPassword(String login, String password) {
		// TODO Auto-generated method stub
		Employe e=null;
		TypedQuery<Employe> req = em.createQuery("select e from Employe e where e.login like :log and e.password like :pwd",Employe.class);
		req.setParameter("log", login).setParameter("pwd", password);
		
		try {
			e = req.getSingleResult();
		} catch (NoResultException e2) {
			// TODO: handle exception
			System.out.println("No data Found");
		}
		return e;
	}

}
