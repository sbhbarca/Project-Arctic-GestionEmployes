package tn.esprit.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import tn.esprit.entities.Employe;
import tn.esprit.entities.Projet;

@Stateless
public class ProjetService implements ProjectInterfaceLocal, ProjectInterfaceRemote {

	@PersistenceContext
	EntityManager entityManager;

	@Override
	public void addProject(Projet p) {
		// TODO Auto-generated method stub
		entityManager.persist(p);
	}

	@Override
	public Projet updateProjet(Projet p) {
		// TODO Auto-generated method stub
		return entityManager.merge(p);
	}

	@Override
	public void deleteProjet(Projet p) {
		// TODO Auto-generated method stub
		entityManager.remove(p);
	}

	@Override
	public List<Projet> getAllProjets() {
		// TODO Auto-generated method stub
		TypedQuery<Projet> query = entityManager.createQuery("select p from Projet p", Projet.class);
		return query.getResultList();
	}

	@Override
	public Projet getProjetById(int id) {
		// TODO Auto-generated method stub
		return entityManager.find(Projet.class, id);
	}

	@Override
	public List<Projet> getProjetsByResponsable(Employe e) {
		// TODO Auto-generated method stub
		TypedQuery<Projet> query = entityManager.createQuery("select p from Projet p where p.employe = :emp ",
				Projet.class);
		query.setParameter("emp", e);
		return query.getResultList();

	}
	
	@Override
	public List<Projet> getProjetsByEmploye(Employe e) {
		// TODO Auto-generated method stub
		TypedQuery<Projet> query = entityManager.createQuery("select p from Projet p join p.taches t where t.employe = :emp",Projet.class);
		query.setParameter("emp", e);
		return query.getResultList();
	}


}
